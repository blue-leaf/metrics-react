import App, { Container } from 'next/app';
import Page from '../components/Page';
import { ApolloProvider } from 'react-apollo';
import withData from '../lib/withData';

// React npm plugins css
import '../node_modules/react-table/react-table.css';

// FontAwesome setup
import { library, config } from '@fortawesome/fontawesome-svg-core';
import { faProjectDiagram, faAlignJustify, faTicketAlt, faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import '@fortawesome/fontawesome-svg-core/styles.css';

// Prevent FoUC of FontAwesome icons
config.autoAddCss = false;

// Add FontAwesome icons to library
library.add(faProjectDiagram, faAlignJustify, faTicketAlt, faArrowDown, faArrowUp);

class BlMetrics extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    // this exposes the query to the user
    pageProps.query = ctx.query;
    return { pageProps };
  }
  render() {
    const { Component, apollo, pageProps } = this.props;

    return (
      <Container>
        <ApolloProvider client={apollo}>
          <Page>
            <Component {...pageProps} />
          </Page>
        </ApolloProvider>
      </Container>
    );
  }
}

export default withData(BlMetrics);
