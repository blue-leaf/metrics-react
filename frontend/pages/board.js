import Board from '../components/Board';

const BoardPage = (props) => (
    <Board id={props.query.id} />
);

export default BoardPage;
