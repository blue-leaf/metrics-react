import Sprint from '../components/Sprint';

const SprintPage = (props) => (
    <Sprint id={props.query.id} />
);

export default SprintPage;
