// Next.js CSS loader, which allows using @import of CSS files
const withCSS = require('@zeit/next-css');
module.exports = withCSS({
    serverRuntimeConfig: { // Will only be available on the server side

    },
    env: { // Will be available on both server and client
        JIRA_HOST: 'https://blue-leaf.atlassian.net'
    },
    target: "server" // Try with serverless before prod deploy
});
