import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Table from "./styles/Table";
import ReactTable from "react-table";
import Router from 'next/router';

const TICKET_COMMITS_QUERY = gql`
  query TICKET_COMMITS_QUERY($id: ID!) {
    ticketCommits(id: $id) {
      pullrequest {
        overall {
          count
          lastUpdated
          stateCount
          state
          dataType
          open
        }
      }
      repository {
        overall {
          count
        }
      }
      branch {
        overall {
          count
        }
      }
    }
  }
`;

class SprintTable extends Component {
  handleClick = (ticketID) => {
    Router.push({
      pathname: '/ticket',
      query: {
        id: ticketID
      }
    })
  }
  render() {
    const { data } = this.props;

    const columns = [
      {
        Header: 'P',
        maxWidth: 34,
        headerClassName: 'center-content',
        className: 'center-content',
        Cell: (row) => {
          return <img width={20} src={row.original.fields.priority.iconUrl} title={row.original.fields.priority.name} />
        }
      }, {
        Header: 'Ticket',
        accessor: 'key',
        maxWidth: 100,
        headerClassName: 'center-content',
        className: 'center-content'
    }, {
      Header: 'Summary',
      maxWidth: 400,
      className: 'has-small-icon',
      Cell: (row) => {
        return <span>
          <img width={20} src={row.original.fields.issuetype.iconUrl} title={row.original.fields.issuetype.name} />
          {row.original.fields.summary}
        </span>
      }
    }, {
      Header: 'Assignee',
      maxWidth: 120,
      headerClassName: 'center-content',
      className: 'center-content',
      Cell: (row) => {
        return <span className={`isActive-${row.original.fields.assignee.active}`}>{row.original.fields.reporter.displayName}</span>
      }
    }, {
      Header: 'Reporter',
      maxWidth: 120,
      headerClassName: 'center-content',
      className: 'center-content',
      Cell: (row) => {
        return <span className={`isActive-${row.original.fields.reporter.active}`}>{row.original.fields.reporter.displayName}</span>
      }
    }, {
      Header: 'Status',
      maxWidth: 200,
      className: 'has-small-icon',
      Cell: (row) => {
        return <span className="ticketStatus">
          <img width={20} src={row.original.fields.status.iconUrl} alt="" title="" />
          {row.original.fields.status.name}
        </span>
      }
    }, {
      Header: 'Development',
      Cell: (row) => {
        return <div className="developmentInfo">
          {row.original.fields.timetracking.originalEstimateSeconds > 0 && <div>Estimated: <strong>{row.original.fields.timetracking.originalEstimate}</strong></div>}
          {row.original.fields.timetracking.timeSpentSeconds > 0 && <div>Time spent: <strong>{row.original.fields.timetracking.timeSpent}</strong></div>}
          {row.original.fields.timetracking.remainingEstimateSeconds > 0 && <div>Remaining: <strong>{row.original.fields.timetracking.remainingEstimate}</strong></div>}
          <Query
            query={TICKET_COMMITS_QUERY}
            variables={{
              id: row.original.id
            }}
            // fetchPolicy="network-only"
          >
            {({ data, error, loading }) => {
              if (loading) return <p>Loading...</p>;
              if (error) return <em>Error fetching dev data</em>;
              return <div>
                {data.ticketCommits.pullrequest.overall.stateCount > 0 ? <div className="pr-status"><span>PR Status: <strong>{data.ticketCommits.pullrequest.overall.state}</strong></span></div> : ''}
                {data.ticketCommits.branch.overall.count > 0 ? <div>Branches: <strong>{data.ticketCommits.branch.overall.count}</strong></div> : ''}
                {data.ticketCommits.repository.overall.count > 0 ? <div>Commits: <strong>{data.ticketCommits.repository.overall.count}</strong></div> : ''}
              </div>;
            }}
          </Query>
        </div>
      }
    }
    ];

    return (
      <Table>
        <ReactTable
          data={data}
          className='styled-table'
          columns={columns}
          pageSizeOptions={[5, 10, 15, 30, 50, 100]}
          defaultPageSize={15}
          defaultSorted={[
            {
              id: "ticket",
              desc: true
            }
          ]}
          minRows={0}
          resizable={false}
          getTdProps={(state, rowInfo, column, instance) => {
            return {
              onClick: (e, handleOriginal) => {
                const sprintID = rowInfo.original.id;
                this.handleClick(sprintID);
              }
            }
          }}
        />
      </Table>
    );
  }
}

export default SprintTable;
