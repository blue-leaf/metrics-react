import React, { Component } from 'react';
import styled, { ThemeProvider, createGlobalStyle } from 'styled-components';
import Header from './Header';
import Left from './Left';
import Meta from './Meta';

const theme = {
  red: '#FF0000',
  black: '#393939',
  blue: '#00BCE4',
  orange: '#FF8800',
  grey: '#363436',
  lightgrey: '#4D4D4D',
  offWhite: '#EDEDED',
  active: 'rgb(75, 192, 192)',
  closed: 'rgb(255, 99, 132)',
  future: 'rgba(255, 159, 64)',
  bs: '0 0 5px 0 rgba(43,43,43,0.1), 0 11px 6px -7px rgba(43,43,43,0.1)',
};

const StyledPage = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: white;
  color: ${props => props.theme.black};
`;

const Inner = styled.div`
  clear: both;
  overflow: hidden;
  display: flex;
  flex: 1 0 auto;
  flex-direction: row;
`;

const Main = styled.main`
  clear: both;
  overflow: hidden;
  flex: 1;
  background: ${props => props.theme.offWhite};
  padding: 30px;
  .pageTitle {
    margin: 0 0 20px;
    font-weight: normal;
  }
`;

const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
    font-size: 10px;
    height: 100%;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    display: flex;
    height: 100%;
    flex-direction: column;
    flex: 1 0 auto;
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 1.8;
    font-family: 'Source Sans Pro', sans-serif;
  }
  #__next {
    height: 100%;
  }
  a {
    text-decoration: none;
    color: ${theme.black};
  }
`;

class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <StyledPage>
          <Meta />
          <Header />
          <Inner>
            <Left />
            <Main>
              {this.props.children}
            </Main>
          </Inner>
          <GlobalStyle />
        </StyledPage>
      </ThemeProvider>
    );
  }
}

export default Page;
