import React, { Component } from 'react';
// import RangeCalendar from 'rc-calendar/lib/RangeCalendar';
import FullCalendar from 'rc-calendar/lib/FullCalendar';
import Select from 'rc-select';
import '../node_modules/rc-calendar/assets/index.css';
import SprintCalendarStyle from './styles/SprintCalendarStyles';
import BlockTitle from './styles/BlockTitle';

import moment from 'moment';

const now = moment();

const defaultCalendarValue = now.clone();
defaultCalendarValue.add(-1, 'month');

const formatStr = 'DD/MM/YYYY';
function format(v) {
  return v ? v.format(formatStr) : '';
}

function onSelect(value) {
  console.log('select', value.format(format));
}

class SprintCalendar extends Component {
  state = {
    type: 'month'
  };

  onTypeChange = (type) => {
    this.setState({
      type
    });
  }

  render() {
    return (
      <SprintCalendarStyle>
        <BlockTitle>Calendar</BlockTitle>
        <FullCalendar
          Select={Select}
          fullscreen
          value={now}
          defaultValue={now}
          onSelect={onSelect}
          type={'days'}
          onTypeChange={this.onTypeChange}
        />
      </SprintCalendarStyle>
    );
  }
}

export default SprintCalendar;
