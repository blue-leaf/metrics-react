import React, { Component } from 'react';
import Router from 'next/router';
import styled from 'styled-components';

const SprintsMapList = styled.tr`
  cursor: pointer;
`;
class SprintsMap extends Component {
  handleClick = () => {
    Router.push({
      pathname: '/sprint',
      query: {
        id: this.props.dataProps.id
      }
    })
  }
  render() {
    return (
      <SprintsMapList onClick={this.handleClick}>
        <td className="sprintName">
          {'name' in this.props.dataProps ? this.props.dataProps.name : ''}
        </td>
        <td className="sprintStartDate">
          {
            'startDate' in this.props.dataProps && this.props.dataProps.startDate
              ? new Date(this.props.dataProps.startDate).toLocaleDateString('en-GB')
              : 'N/A'
          }
        </td>
        <td className="sprintEndDate">
          {
            'endDate' in this.props.dataProps && this.props.dataProps.endDate
              ? new Date(this.props.dataProps.endDate).toLocaleDateString('en-GB')
              : 'N/A'
          }
        </td>
        <td className={`sprintState ${'state' in this.props.dataProps ? this.props.dataProps.state : ''}`}>
          <span>{'state' in this.props.dataProps ? this.props.dataProps.state : ''}</span>
        </td>
      </SprintsMapList>
    );
  }
}

export default SprintsMap;
