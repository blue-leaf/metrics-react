import React, { Component } from 'react';
import Table from "./styles/Table";
import ReactTable from "react-table";
import Router from 'next/router';
import moment from 'moment';

class BoardTable extends Component {
  handleClick = (sprintID) => {
    Router.push({
      pathname: '/sprint',
      query: {
        id: sprintID
      }
    })
  }
  render() {
    const columns = [{
      Header: 'Name',
      accessor: 'name',
      minWidth: 400
    }, {
      Header: 'Start Date',
      accessor: 'startDate',
      headerClassName: 'center-content',
      className: 'center-content',
      Cell: props => props.value !== null
        ? moment(props.value).format('DD/MM/YYYY')
        : 'N/A'
    }, {
      Header: 'End Date',
      accessor: 'endDate',
      headerClassName: 'center-content',
      className: 'center-content',
      Cell: props => props.value !== null
        ? moment(props.value).format('DD/MM/YYYY')
        : 'N/A'
    }, {
      Header: 'Status',
      accessor: 'state',
      headerClassName: 'center-content',
      className: 'center-content',
      Cell: props => <span className={`status state-${props.value}`}>{props.value}</span>
    }];

    const { data } = this.props;
    return (
      <Table>
        <ReactTable
          data={data}
          className='styled-table'
          columns={columns}
          pageSizeOptions={[5, 10, 15, 30, 50, 100]}
          defaultPageSize={15}
          defaultSorted={[
            {
              id: "startDate",
              desc: true
            }
          ]}
          minRows={0}
          resizable={false}
          getTdProps={(state, rowInfo, column, instance) => {
            return {
              onClick: (e, handleOriginal) => {
                const sprintID = rowInfo.original.id;
                this.handleClick(sprintID);
              }
            }
          }}
        />
      </Table>
    );
  }
}

export default BoardTable;
