import Link from 'next/link';
import styled from 'styled-components';
import NProgress from 'nprogress';
import Nav from './Nav';

const StyledLeft = styled.aside`
  display: flex;
  flex-direction: column;
  clear: both;
  overflow: hidden;
  flex: 0 0 25rem;
  padding: 30px 0;
  background: ${props => props.theme.grey};
  color: white;
`;

const Left = () => (
  <StyledLeft>
    <Nav />
  </StyledLeft>
);

export default Left;
