import Link from 'next/link';
import styled from 'styled-components';
import Router from 'next/router';
import NProgress from 'nprogress';

Router.onRouteChangeStart = () => {
  NProgress.start();
};

Router.onRouteChangeComplete = () => {
  NProgress.done();
};

Router.onRouteChangeError = () => {
  NProgress.done();
};

const Logo = styled.h1`
  margin: 0;
  padding: 1rem;
  width: 25rem;
  a {
    display: block;
    img {
      max-width: 15rem;
      display: block;
      margin: 0 auto;
    }
  }
`;

const StyledHeader = styled.header`
  position: relative;
  flex: none;
  box-shadow: ${props => props.theme.bs};
  .bar {
    display: flex;
    justify-content: space-between;
    align-items: stretch;
  }
`;

const Header = () => (
  <StyledHeader>
    <div className="bar">
      <Logo>
        <Link href="/" prefetch>
          <a><img src="/static/logo.png" alt="BlueLeaf" /></a>
        </Link>
      </Logo>
    </div>
  </StyledHeader>
);

export default Header;
