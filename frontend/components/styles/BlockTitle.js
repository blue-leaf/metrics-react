import styled from 'styled-components';

const BlockTitle = styled.h3`
  margin: 0 0 0.5rem;
  padding: 0 0 0.5rem;
  font-size: 2rem;
  font-weight: 500;
  position: relative;
  &:after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 50px;
    height: 1px;
    background: ${props => props.theme.blue};
  }
`;

export default BlockTitle;
