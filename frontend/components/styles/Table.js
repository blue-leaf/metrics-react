import styled from 'styled-components';
import { renderToString } from 'react-dom/server';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Table = styled.div`
  padding: 1rem 1rem 0;
  .styled-table {
    border: 0;
    .rt-thead {
      font-weight: bold;
      &.-header {
        box-shadow: none;
      }
      .rt-tr {
        text-align: left;
        border-bottom: dashed 1px rgba(0,0,0,0.15);
      }
      .rt-th:focus {
        outline: none;
      }
      .rt-th,
      .rt-td {
        box-shadow: none;
        border: 0;
        &.center-content {
          text-align: center;
        }
        &.-sort-asc {
          div {
            &:after {
              content: '';
              background: url(data:image/svg+xml,${encodeURIComponent(renderToString(<FontAwesomeIcon icon={ ['fas', 'arrow-up'] } color="#393939" />))});
              background-size: 12px auto;
              background-position: center center;
              display: inline-block;
              width: 12px;
              height: 12px;
              vertical-align: middle;
              margin-left: 6px;
            }
          }
        }
        &.-sort-desc {
          div {
            &:after {
              content: '';
              background: url(data:image/svg+xml,${encodeURIComponent(renderToString(<FontAwesomeIcon icon={ ['fas', 'arrow-down'] } color="#393939" />))});
              background-size: 12px auto;
              background-position: center center;
              display: inline-block;
              width: 12px;
              height: 12px;
              vertical-align: middle;
              margin-left: 8px;
            }
          }
        }
      }
    }
    .rt-tbody {
      .rt-tr-group {
        cursor: pointer;
        border-bottom: dashed 1px rgba(0,0,0,0.15);
        &:hover {
          background: #f2f2f2;
        }
        &:last-child {
          th, td {
            border-bottom: 0;
          }
        }
        .rt-td {
          box-shadow: none;
          border: 0;
          &.center-content {
            text-align: center;
          }
          &.has-small-icon {
            img {
              display: inline-block;
              vertical-align: middle;
              margin-right: 5px;
            }
          }
          .status {
            display: inline;
            padding: 2px 5px;
            border-radius: 3px;
            color: white;
            &.state-active {
              background-color: ${props => props.theme.active};
            }
            &.state-closed {
              background-color: ${props => props.theme.closed};
            }
            &.state-future {
              background-color: ${props => props.theme.future};
            }
          }
          .developmentInfo {
            line-height: 18px;
            .pr-status {
              span {
                display: inline-block;
                border-top: dashed 1px rgba(0,0,0,0.15);
                margin-top: 5px;
                padding-top: 5px;
              }
            }
          }
        }
      }
    }
    .-pagination {
      box-shadow: none;
      border: 0;
      margin-top: 1rem;
      padding: 0;
      .-btn {
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 1.5rem;
        color: ${props => props.theme.black};
      }
    }
  }
`;

export default Table;
