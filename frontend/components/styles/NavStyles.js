import styled from 'styled-components';

const NavStyles = styled.nav`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  a {
    background: none;
    display: block;
    position: relative;
    padding: 1rem 1rem 1rem 4.5rem;
    color: rgba(255, 255, 255, 0.7);
    font-size: 1.5rem;
    line-height: 1.8rem;
    border-left: solid 3px transparent;
    cursor: pointer;
    &:hover,
    &:focus {
      outline: none;
      color: white;
    }
    &.active {
      background: ${props => props.theme.lightgrey};
      border-color: ${props => props.theme.orange};
      color: white;
    }
    .svg-inline--fa {
      position: absolute;
      left: 12px;
      top: 50%;
      transform: translateY(-50%);
      display: inline-block;
      vertical-align: middle;
      margin-right: 0.5rem;
    }
  }
`;

export default NavStyles;
