import styled from 'styled-components';

const DoughnutStyle = styled.div`
  background: white;
  padding: 10px;
  border-radius: 5px;
  box-shadow: ${props => props.theme.bs};
`;

export default DoughnutStyle;
