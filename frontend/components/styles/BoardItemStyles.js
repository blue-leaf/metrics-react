import styled from 'styled-components';

const BoardItem = styled.div`
  background: white;
  border: solid 1px #e6e6f2;
  padding: 5px;
  border-radius: 5px;
  box-shadow: ${props => props.theme.bs};
  a {
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
  }
  img {
    margin-right: 1rem;
    max-width: 24px;
    height: auto;
  }
`;

export default BoardItem;
