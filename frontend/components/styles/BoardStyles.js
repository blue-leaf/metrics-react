import styled from 'styled-components';

const BoardStyle = styled.div`
  display: flex;
  flex-direction: row;
  border: 0;
  .allSprints {
    background: white;
    flex: 3 0 0;
    margin-right: 30px;
    padding: 10px;
    border-radius: 5px;
    box-shadow: ${props => props.theme.bs};
    h3 {
      margin-left: 1rem;
    }
    table {
      width: 100%;
      tr {
        &:hover {
          td {
            background: #f2f2f2;
          }
        }
        &:last-child {
          th, td {
            border-bottom: 0;
          }
        }
      }
      th, td {
        text-align: center;
        border: 0;
        border-bottom: dashed 1px rgba(0,0,0,0.15);
        padding: 5px 10px;
      }
      .sprintName {
        text-align: left;
      }
      .sprintState {
        span {
          padding: 2px 5px;
          border-radius: 2px;
          color: white;
        }
        &.active {
          span {
            background: ${props => props.theme.active};
          }
        }
        &.closed {
          span {
            background: ${props => props.theme.closed};
          }
        }
        &.future {
          span {
            background: ${props => props.theme.future};
          }
        }
      }
    }
  }
  .rightCol {
    flex: 2 0 0;
  }
  .doughnutChart {
    margin-bottom: 3rem;
  }
`;

export default BoardStyle;
