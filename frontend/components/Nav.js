import Link from './Link';
import NavStyles from './styles/NavStyles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Nav = () => (
  <NavStyles>
    <Link activeClassName='active' href='/'>
      <a><FontAwesomeIcon icon='project-diagram' /> Boards</a>
    </Link>
    <Link activeClassName='active' href='/sprints'>
      <a><FontAwesomeIcon icon='align-justify' /> Sprints</a>
    </Link>
    <Link activeClassName='active' href='/tickets'>
      <a><FontAwesomeIcon icon='ticket-alt' /> Tickets</a>
    </Link>
  </NavStyles>
);

export default Nav;
