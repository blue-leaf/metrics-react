import React, { Component } from 'react';
import styled from 'styled-components';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import BoardItem from './BoardItem';
import Error from './ErrorMessage';
// import { perPage } from '../config';

const ALL_BOARDS_QUERY = gql`
  query {
    boards {
      id
      type
      location {
        name
        avatarURI
      }
    }
  }
`;

const Center = styled.div`
  text-align: center;
`;

const ItemsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-gap: 30px;
  margin: 0 auto;
`;

class Boards extends Component {
  render() {
    return (
      <Center>
        <Query
          query={ALL_BOARDS_QUERY}
          // fetchPolicy="network-only"
        >
          {({ data, error, loading }) => {
            if (loading) return <p>Loading...</p>;
            if (error) return <Error error={error} />;
            return <ItemsList>{data.boards.map(({ id, type, location }) => (
              <BoardItem
                key={id}
                id={id}
                type={type}
                avatar={location.avatarURI}
                name={location.name}
              />
            ))}</ItemsList>;
          }}
        </Query>
      </Center>
    );
  }
}

export default Boards;
export { ALL_BOARDS_QUERY };
