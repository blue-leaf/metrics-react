import React, { Component } from 'react';
import _ from 'lodash';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Head from 'next/head';
import BoardStyle from './styles/BoardStyles';
import BlockTitle from './styles/BlockTitle';
import SprintTable from './SprintTable';
import Error from './ErrorMessage';

const SINGLE_SPRINT_QUERY = gql`
  query SINGLE_SPRINT_QUERY($id: ID!) {
    sprint(id: $id) {
      id
      key
      fields {
        components {
          name
        }
        issuetype {
          name
          iconUrl
        }
        priority {
          name
          iconUrl
        }
        summary
        assignee {
          displayName
          active
        }
        reporter {
          displayName
          active
        }
        status {
          iconUrl
          name
          statusCategory {
            name
            colorName
          }
        }
        timetracking {
          originalEstimate
          timeSpent
          remainingEstimate
          originalEstimateSeconds
          remainingEstimateSeconds
          timeSpentSeconds
        }
      }
    }
    sprintName(id: $id) {
      originBoardId
      name
    }
  }
`;

class Sprint extends Component {
  render() {
    const { id } = this.props;
    return (
      <Query
        query={SINGLE_SPRINT_QUERY}
        variables={{
          id: id
        }}
        // fetchPolicy="network-only"
      >
        {({ data, error, loading }) => {
          if (loading) return <p>Loading...</p>;
          if (error) return <Error error={error} />;
          let sprintData = data.sprint;
          return <BoardStyle>
            <Head>
              <title>
                {`${data.sprintName.name} - BlueLeaf Metrics`}
              </title>
            </Head>
            <div className="allSprints">
              <BlockTitle>{data.sprintName.name}</BlockTitle>
              <SprintTable data={sprintData} />
            </div>
          </BoardStyle>;
        }}
      </Query>
    );
  }
}

export default Sprint;
export { SINGLE_SPRINT_QUERY };
