import Link from './Link';
import BoardItemStyles from './styles/BoardItemStyles';
import getConfig from 'next/config';
const {publicRuntimeConfig} = getConfig();

class BoardItem extends React.Component {
  render() {
    const { ...props } = this.props;
    return (
      <BoardItemStyles>
        <Link
          prefetch
          href={{
            pathname: '/board',
            query: { id: props.id }
          }}
        >
          <a>
            <img src={process.env.JIRA_HOST + props.avatar} title={props.name} /> {props.name}
          </a>
        </Link>
      </BoardItemStyles>
     );
  }
}

export default BoardItem;
