import React, { Component } from 'react';
import {Doughnut} from 'react-chartjs-2';
import DoughnutStyle from '../styles/DoughnutStyles';
import BlockTitle from '../styles/BlockTitle';

class DoughnutCutout extends Component {
  render() {
    return (
      <DoughnutStyle>
        <BlockTitle>Sprints statuses</BlockTitle>
        <Doughnut
          data={this.props.data}
          options={this.props.options}
        />
      </DoughnutStyle>
    );
  }
}

export default DoughnutCutout;
