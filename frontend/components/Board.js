import React, { Component } from 'react';
import _ from 'lodash';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Head from 'next/head';
import BoardStyle from './styles/BoardStyles';
import BlockTitle from './styles/BlockTitle';
import Error from './ErrorMessage';
import BoardTable from './BoardTable';
import DoughnutCutout from './charts/DoughnutCutout';
import SprintCalendar from './SprintCalendar';


const SINGLE_BOARD_QUERY = gql`
  query SINGLE_BOARD_QUERY($id: ID!) {
    board(id: $id) {
      id
      name
      state
      startDate
      endDate
      goal
    }
    boardName(id: $id) {
      location {
        name
      }
    }
  }
`;

class Board extends Component {
  // Return count of sprints with selected state
  getSprintStateCount(data) {
    return _.pick(_.countBy(data, 'state'),  ["closed", "active", "future"]);
  }
  render() {
    const { id } = this.props;
    return (
      <Query
        query={SINGLE_BOARD_QUERY}
        variables={{
          id: id
        }}
        // fetchPolicy="network-only"
      >
        {({ data, error, loading }) => {
          if (loading) return <p>Loading...</p>;
          if (error) return <Error error={error} />;
          let boardData = data.board;
          let closedSprintsCount = this.getSprintStateCount(boardData).closed;
          let activeSprintsCount = this.getSprintStateCount(boardData).active;
          let futureSprintsCount = this.getSprintStateCount(boardData).future;

          let chartData = {
            datasets: [{
              data: [closedSprintsCount,activeSprintsCount,futureSprintsCount],
              backgroundColor: [
                "rgb(75, 192, 192)",
                "rgb(255, 99, 132)",
                "rgba(255, 159, 64)"
              ]
            }],
            labels: [
              'Closed',
              'Active',
              'Future'
            ]
          }
          let chartOptions = {
            cutoutPercentage: 50,
            circumference: Math.PI,
            rotation: -Math.PI,
            legend: {
              position: 'bottom',
            }
          }
          return <BoardStyle>
            <Head>
              <title>
                {`${data.boardName.location.name} - BlueLeaf Metrics`}
              </title>
            </Head>
            <div className="allSprints">
              <BlockTitle>{data.boardName.location.name}</BlockTitle>
              <BoardTable data={boardData} />
            </div>
            <div className="rightCol">
              <div className="doughnutChart">
                <DoughnutCutout data={chartData} options={chartOptions} />
              </div>
              <div className="sprintCalendar">
                <SprintCalendar />
              </div>
            </div>
          </BoardStyle>;
        }}
      </Query>
    );
  }
}

export default Board;
export { SINGLE_BOARD_QUERY };
