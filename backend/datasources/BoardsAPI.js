const { RESTDataSource } = require('apollo-datasource-rest');
require('dotenv').config();

class BoardsAPI extends RESTDataSource {
    willSendRequest(request) {
        request.headers.set('Authorization', `Basic ${this.context.token}`);
    }

    constructor() {
        super();
        this.baseURL = `${process.env.JIRA_HOST}`;
    }

    async getAllBoards() {
        return this.get(`/${process.env.ENDPOINT}/board`);
    }

    async getBoardName(id) {
        return this.get(`/${process.env.ENDPOINT}/board/${id}`);
    }

    async getBoard(id) {
        return this.get(`/${process.env.ENDPOINT}/board/${id}/sprint`);
    }

    async getSprintTickets(id) {
        return this.get(`/${process.env.ENDPOINT}/sprint/${id}/issue`);
    }

    async getSprintName(id) {
        return this.get(`/${process.env.ENDPOINT}/sprint/${id}`);
    }

    async getTicket(id) {
        return this.get(`/${process.env.ENDPOINT}/issue/${id}`);
    }

    async getTicketCommits(id) {
        return this.get(`/${process.env.BB_ENDPOINT}/issue/summary?issueId=${id}`);
    }
}

module.exports = BoardsAPI;
