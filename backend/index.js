const express = require('express');
const { ApolloServer, makeExecutableSchema, gql } = require('apollo-server-express');
const { importSchema } = require('graphql-import');
const cors = require('cors');
const app = express();
const BoardsAPI = require('./datasources/BoardsAPI');

require('dotenv').config();

const AUTH_HEADER = Buffer.from(`${process.env.JIRA_USER}:${process.env.PASS}`).toString('base64');

// Import GraphQL type definitions
const typeDefs = importSchema('src/schema.graphql');

// Fetching schema types
const resolvers = {
  Query: {
    boards: async (_source, _args, { dataSources }) => {
      return dataSources.boards.getAllBoards()
        .then(
          data => data.values
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    },
    board: async (_source, { id }, { dataSources }) => {
      return dataSources.boards.getBoard(id)
        .then(
          data => data.values
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    },
    boardName: async (_source, { id }, { dataSources }) => {
      return dataSources.boards.getBoardName(id)
        .then(
          data => data
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    },
    sprint: async (_source, { id }, { dataSources }) => {
      return dataSources.boards.getSprintTickets(id)
        .then(
          data => data.issues
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    },
    sprintName: async (_source, { id }, { dataSources }) => {
      return dataSources.boards.getSprintName(id)
        .then(
          data => data
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    },
    ticket: async (_source, { id }, { dataSources }) => {
      return dataSources.boards.getTicket(id)
        .then(
          data => data.fields
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    },
    ticketCommits: async (_source, { id }, { dataSources }) => {
      return dataSources.boards.getTicketCommits(id)
        .then(
          data => data.summary
        )
        .catch((err) => {
          console.log('Error fetching data: ' + err);
        });
    }
  }
};

const corsOptions = {
  origin: process.env.FRONTEND_URL,
  credentials: true,
  methods: ['GET', 'PUT', 'POST', 'OPTIONS'],
};

app.use(cors(corsOptions));

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

// Start Apollo Server
// Add dataSources
// Pass context to requests
const server = new ApolloServer({
    schema,
    dataSources: () => {
      return {
        boards: new BoardsAPI()
      }
    },
    context: () => {
      return {
        token: AUTH_HEADER
      };
    },
    /* graphql-upload is not working on Node <= 8.5 and we don't need file uploads */
    uploads: false
});

server.applyMiddleware({
  app,
  path: '/',
  cors: corsOptions
});

// Launch web server
app.listen({ port: process.env.PORT }, () => {
  console.log(`🚀  Server ready at http://localhost:${process.env.PORT}`);
});