# metrics-electron-app
Tool to gather metrics from Jira and Bitbucket for BlueLeaf team

## Dev info
1. `cd backend`
    1. `npm install` - install all dependenices
    2. `npm run server` - start Apollo Server on https://localhost:4000
2. `cd frontend`
    1. `npm install` - install all dependenices
    2. `npm run dev` - start React App on https://localhost:7777

## Roadmap
* General styling
* App deployment with [Now](https://zeit.co/now)
* Start both servers with single command: `npm-run-all` - `run-s` command
* Implement [Schema Stitching](https://www.apollographql.com/docs/graphql-tools/schema-stitching.html) for Sprint/Sprint tickets queries

## Useful links
* [Generate API token for JIRA user](https://id.atlassian.com/manage/api-tokens)